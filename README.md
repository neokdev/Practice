# Symfony - Practice

In order to use this project locally, please be sure to have Make installed.

## Usage

In order to use this project, you should have Docker & Docker-compose installed, 
the last requirements is to use [Google Cloud Platform](https://cloud.google.com/) (along with the GCP SDK), 
here, the GCP is used for building and API purpose. 

Once you've installed Docker, time to build the project.

This project use Docker environment files in order to allow the configuration according to your needs,
this way, you NEED to define a .env file in order to launch the build.

**_In order to perform better, Docker can block your dependencies installation and return an error
or never change your php configuration, we recommend to delete all your images/containers
before building the project_**

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -a -q) -f
```

## Installation

The project should be installed using Make, be sure to have configured the `.env` file using `cp .env.dist .env`

```bash
cp .env.dist .env
make start
```
